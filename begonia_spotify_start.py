import spotify_merge
import configparser
import json
import spotipy
from loguru import logger


def merge(sp, data):
    for target_playlist_id in data:
        target_playlist = sp.playlist(target_playlist_id)
        for source_playlist_id in data[target_playlist_id]:
            source_playlist = sp.playlist(source_playlist_id)
            # spotify API is limited to 100 tracks, for longer playlists API has to be called multiple times
            spotify_merge.playlist_longify(sp, target_playlist)
            spotify_merge.playlist_longify(sp, source_playlist)
            spotify_merge.merge_playlists(sp, target_playlist, source_playlist)


def spotipy_authenticate(scope, client_id, client_secret, redirect_uri):
    auth_manager = spotipy.oauth2.SpotifyOAuth(scope=scope, client_id=client_id, client_secret=client_secret,
                                               redirect_uri=redirect_uri)
    return spotipy.Spotify(auth_manager=auth_manager)


if __name__ == '__main__':
    logger.add("logs/begonia_{time}.log")
    # reading the config/settings from file
    config = configparser.ConfigParser()
    config.read('settings.ini')
    scope = config['DEFAULT']['scope']
    client_id = config['DEFAULT']['client_id']
    client_secret = config['DEFAULT']['client_secret']
    redirect_uri = config['DEFAULT']['redirect_uri']

    # authenticate with spotify
    sp = spotipy_authenticate(scope, client_id, client_secret, redirect_uri)

    # read playlists to be merged from file
    with open('playlists.json') as playlists_json:
        data = json.load(playlists_json)
    merge(sp, data)
