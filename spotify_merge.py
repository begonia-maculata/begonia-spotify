from loguru import logger


# since the Spotify API only returns the first 100 tracks of a playlist, it has to be called multiple times for a
# playlist longer than 100 tracks
def playlist_longify(sp, playlist):
    playlist_tracks = playlist['tracks']
    while playlist_tracks['next']:
        next_tracks = sp.next(playlist_tracks)
        playlist['tracks']['items'].extend(next_tracks['items'])
        playlist_tracks = next_tracks


# given a list longer than n, returns a list of lists with each sublist n (the last sublist might be shorter)
# if the given list has less equal than n items, a list of list is still returned
def divide_chunks(list_in, chunk_len):
    result = []
    for i in range(0, len(list_in), chunk_len):
        result.append(list_in[i:i + chunk_len])
    return result


# adds all tracks from the source playlist to the destination playlist
def merge_playlists(sp, target_playlist, source_playlist):
    source_name = source_playlist['name']
    target_name = target_playlist['name']

    # get track ids from source and target playlist
    source_playlist_track_ids = []
    for item in source_playlist['tracks']['items']:
        source_playlist_track_ids.append(item['track']['uri'])
    target_playlist_track_ids = []
    for item in target_playlist['tracks']['items']:
        target_playlist_track_ids.append(item['track']['uri'])

    # get all the track ids that are included in source but not in target
    tracks_to_add = [track_id for track_id in source_playlist_track_ids if track_id not in target_playlist_track_ids]
    if len(tracks_to_add) == 0:
        logger.info('No tracks in "' + source_name + '" that aren\'t already in "' + target_name + '"')
    else:
        logger.info('Merging "' + source_name + '" -> "' + target_name + '"')
        for tracks_to_add_chunk in divide_chunks(tracks_to_add, 100):
            sp.playlist_add_items(target_playlist['uri'], tracks_to_add_chunk)
