# Begonia Spotify

## Introduction

Begonia Spotify is a python program that allows you to automatically merge Spotify playlists into other Spotify playlists. The idea is to have a bigger playlist that consists of multiple smaller subplaylists. Lets say you have a "Indie Rock", "Classic Rock" and a "Hard Rock" playlist. Now it would be quite reasonable to have a bigger "Rock" playlist that consists of all the aforementioned playlists, wouldn't it? Since Begonia Spotify is a python script you can automatically execute it every few minutes (via cronjob for example) so any song you add to any of your playlist also gets added to the bigger playlist.

## Requirements

### Spotipy

Install with ``pip install spotipy --upgrade`` or ``pip3 install spotipy --upgrade``

More info [here](https://spotipy.readthedocs.io/en/2.17.1/#)

### loguru

Install with ``pip install loguru`` or ``pip3 install loguru``

More info [here](https://pypi.org/project/loguru/)

## Setup

### settings.ini

Create a ``settings.ini`` file in the root folder of the project, copy and paste the following text and fill in your ``client_id`` and ``client_secret`` from spotify (without quotation marks). Instructions on how to get a ``client_id`` and ``client_secret`` can be found [here.](https://developer.spotify.com/documentation/general/guides/app-settings/)

```config
[DEFAULT]
scope = playlist-modify-private
client_id = your_spotify_client_id
client_secret = your_spotify_client_secret
redirect_uri = http://localhost:8888/callback
```

### playlists.json

The information which playlists should be merged into which gets specified in the ``playlists.json`` file. Create the file and insert your playlist URIs to be merged in the following structure (with quotation marks):

```json
{
  "Target Playlist 1 URI": [
    "Sub Playlist 1.1 URI",
    "Sub Playlist 1.2 URI",
    "Sub Playlist 1.3 URI"
  ],
  "Target Playlist 2 URI": [
    "Sub Playlist 2.1 URI",
    "Sub Playlist 2.2 URI"
  ]
}

```

Attention: Currently playlists with local files (tracks that aren't on spotify) will cause the script not to work.

## Start

Run the ``begonia_spotify_start.py`` without any parameters

